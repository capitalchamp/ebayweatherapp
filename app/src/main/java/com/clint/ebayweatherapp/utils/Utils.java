package com.clint.ebayweatherapp.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;

import com.clint.ebayweatherapp.WeatherApplication;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Clint on 2016-05-15.
 */
public class Utils {

    public static String capitalizeString(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }

        if (!input.substring(0, 1).equals(" ")) {
            input = input.substring(0, 1).toUpperCase() + input.substring(1, input.length());
        }

        for (int i = 0; i < input.length(); i++) {
            if (input.substring(i, i+1).equals(" ") && input.length() >= i+2) {
                input = input.substring(i+1, i+2).toUpperCase() + input.substring(i+2, input.length());
            }
        }
        return input;
    }

	public static void addMaliciousCharsFilterToEditText(EditText editText) {

		final InputFilter maliciousCharsFilter = new InputFilter() {

			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                Character [] maliciousChars =  new Character[] { '<', '>', '%', '{', '}', '[', ']', '&', '#', '\\', '"', '|', '/' };
				Set<Character> unAcceptedChars = new HashSet<>();
                Collections.addAll(unAcceptedChars, maliciousChars);

				String badCharsStr = "";

				for (char badChar : unAcceptedChars) {
					badCharsStr = badCharsStr + badChar;
				}

				for (int index = start; index < end; index++) {
					if (badCharsStr.contains(String.valueOf(source.charAt(index)))) {
						return "";
					}
				}

				return null;
			}
		};

		InputFilter[] newInputFilters;
		InputFilter[] currentInputFilters = editText.getFilters();

		if ((currentInputFilters != null) && (currentInputFilters.length > 0)) {
			newInputFilters = new InputFilter[currentInputFilters.length + 1];
			System.arraycopy(currentInputFilters, 0, newInputFilters, 0, currentInputFilters.length);
			newInputFilters[newInputFilters.length - 1] = maliciousCharsFilter;

		} else {
			newInputFilters = new InputFilter[] { maliciousCharsFilter };
		}

		editText.setFilters(newInputFilters);
	}

    public static int kelvinToCelsius(double temperature) {
        // Given in Kelvin. This function will show it in Celsius
        double celsius = temperature - 273.0;
        return  (int)celsius;
    }
}
