package com.clint.ebayweatherapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.clint.ebayweatherapp.R;
import com.clint.ebayweatherapp.servicesapi.converters.WeatherDataConverter;
import com.clint.ebayweatherapp.servicesapi.RestManager;
import com.clint.ebayweatherapp.servicesapi.models.dtos.DtoWeatherData;
import com.clint.ebayweatherapp.types.IconType;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.RecentSearch;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.RecentSearches;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.WeatherViewModel;
import com.clint.ebayweatherapp.utils.Utils;
import com.clint.ebayweatherapp.views.WeatherLabelDataView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivity extends WeatherDrawerActivity implements   GoogleApiClient.ConnectionCallbacks,
                                                                        GoogleApiClient.OnConnectionFailedListener,
                                                                        LocationListener,
                                                                        Callback<DtoWeatherData> {
    private static final String TAG = WeatherActivity.class.getSimpleName();
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final String RECENT_SEARCHES_KEY = "RECENT_SEARCHES_KEY";
    private static final String WEATHER_DATA_KEY = "WEATHER_DATA_KEY";
    private static final String RECENT_SEARCHES_BOOLEAN = "RECENT_SEARCHES_BOOLEAN";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 444;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    protected Location mCurrentLocation;

    private Gson gson = new Gson();

    private WeatherViewModel mWeatherViewModel;
    private boolean areThereRecentSearches = false;

    /**
     *  UI Elements
     */
    private View mContentView;
    private ImageView mBackgroundIV;
    private ImageView mIconIV;
    private TextView mTitleTV;
    private TextView mStatusTV;
    private TextView mTempTV;
    private WeatherLabelDataView mTempRangeView;
    private WeatherLabelDataView mPressureView;
    private WeatherLabelDataView mHumidityView;
    private WeatherLabelDataView mWindSpeedView;
    private WeatherLabelDataView mCloudPctView;
    private WeatherLabelDataView mSunriseView;
    private WeatherLabelDataView mSunsetView;
    private WeatherLabelDataView mDataTimeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUiElements();

        initializeGoogleLocationServices();
        updateRecentSearchesList();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mWeatherViewModel != null) {
            outState.putSerializable(WEATHER_DATA_KEY, mWeatherViewModel);
        }
        outState.putBoolean(RECENT_SEARCHES_BOOLEAN, areThereRecentSearches);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mWeatherViewModel = (WeatherViewModel) savedInstanceState.getSerializable(WEATHER_DATA_KEY);
            if (mWeatherViewModel != null) {
                updateContent(mWeatherViewModel);
            }
            areThereRecentSearches = savedInstanceState.getBoolean(RECENT_SEARCHES_BOOLEAN, false);
        }
    }

    private void setupUiElements() {
        mContentView = findViewById(R.id.content);
        mBackgroundIV = (ImageView)findViewById(R.id.weather_background);
        mIconIV = (ImageView)findViewById(R.id.weather_icon);
        mTitleTV = (TextView)findViewById(R.id.weather_title);
        mStatusTV = (TextView)findViewById(R.id.weather_status);
        mTempTV = (TextView)findViewById(R.id.weather_temp);
        mTempRangeView = (WeatherLabelDataView)findViewById(R.id.temp_range);
        mPressureView = (WeatherLabelDataView)findViewById(R.id.pressure);
        mHumidityView = (WeatherLabelDataView)findViewById(R.id.humidity);
        mWindSpeedView = (WeatherLabelDataView)findViewById(R.id.wind_speed);
        mCloudPctView = (WeatherLabelDataView)findViewById(R.id.cloud_percentage);
        mSunriseView = (WeatherLabelDataView)findViewById(R.id.sunrise_time);
        mSunsetView = (WeatherLabelDataView)findViewById(R.id.sunset_time);
        mDataTimeView = (WeatherLabelDataView)findViewById(R.id.data_collected);
    }

    private void initializeGoogleLocationServices() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest()
                .setInterval(INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (checkLocationPermission()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void updateRecentSearchesList() {
        RecentSearches recentSearchesObj;
        if ((recentSearchesObj = getRecentSearchesFromPreferences()) != null && recentSearchesObj.getMostRecentSearch() != null) {
            areThereRecentSearches = true;
            super.initRecentSearchesDrawerList(recentSearchesObj);

            RestManager.getInstance().getWeatherById(recentSearchesObj.getMostRecentSearch().getId(), this);
        }
    }

    protected RecentSearches getRecentSearchesFromPreferences() {
        SharedPreferences recentSearches = getPreferences(Context.MODE_PRIVATE);
        if (recentSearches == null) {
            return null;
        }
        String recentSearchesJson = recentSearches.getString(RECENT_SEARCHES_KEY, null);
        if (recentSearchesJson != null) {
            return gson.fromJson(recentSearchesJson, RecentSearches.class);
        } else {
           return null;
        }
    }

    protected void setRecentSearchToPreferences(RecentSearch recentSearch) {
        RecentSearches recentSearchesObj = getRecentSearchesFromPreferences();

        if (recentSearchesObj == null) {
            recentSearchesObj = new RecentSearches();
        }
        recentSearchesObj.addRecentSearch(recentSearch);
        super.initRecentSearchesDrawerList(recentSearchesObj);

        String recentSearchesStr = gson.toJson(recentSearchesObj);

        SharedPreferences recentSearches = getPreferences(Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = recentSearches.edit();
        editor.putString(RECENT_SEARCHES_KEY, recentSearchesStr);
        editor.apply();
    }

    protected void setRecentSearchesToPreferences(RecentSearches recentSearches) {
        if (recentSearches == null) {
            return;
        }
        super.initRecentSearchesDrawerList(recentSearches);
        String recentSearchesStr = gson.toJson(recentSearches);
        SharedPreferences recentSearchesPref = getPreferences(Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = recentSearchesPref.edit();
        editor.putString(RECENT_SEARCHES_KEY, recentSearchesStr);
        editor.apply();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {
        }
        Log.d(TAG, "Location update started");
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;

        Log.d(TAG, "Location changed, LatLng: " + location.getLatitude() + "," + location.getLongitude());

        if (!areThereRecentSearches) {
            areThereRecentSearches = true;
            Menu menu = super.getMenu();
            MenuItem menuItem = menu.findItem(R.id.nav_gps);
            if (menuItem != null) {
                menuItem.setChecked(true);
                RestManager.getInstance().getWeatherByLatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), this);
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mContentView != null) {
            Snackbar.make(mContentView, R.string.location_could_not_connect, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onResponse(Call<DtoWeatherData> call, Response<DtoWeatherData> response) {
        if (response.isSuccessful()) {
            Log.d(TAG, "Successful Response: " + response.body().toString());
            try {
                DtoWeatherData weatherData = response.body();

                RecentSearch recentSearch = new RecentSearch(weatherData.getName(), weatherData.getSys().getCountry(), weatherData.getId());
                setRecentSearchToPreferences(recentSearch);

                mWeatherViewModel = WeatherDataConverter.createWeatherViewModel(weatherData);

                updateContent(mWeatherViewModel);
            } catch (Exception e) {
                Log.d(TAG, "Data error from Open Weather");
            }
        } else {
            if (mContentView != null) {
                Snackbar.make(mContentView, R.string.weather_could_not_fetch, Snackbar.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onFailure(Call<DtoWeatherData> call, Throwable t) {
        Log.d(TAG, t.getMessage());
        if (mContentView != null) {
            Snackbar.make(mContentView, R.string.weather_could_not_fetch, Snackbar.LENGTH_SHORT);
        }
    }

    private void updateContent(WeatherViewModel weatherViewModel) {
        updateBackgroundAndIcon(weatherViewModel.getIcon());

        String cityAndCountry = weatherViewModel.getCity() + ", " + weatherViewModel.getCountry();
        String tempurature = getTemperatureString(weatherViewModel.getTemp());
        String temperatureRange = getTemperatureString(weatherViewModel.getMinTemp()) + " to " + getTemperatureString(weatherViewModel.getMaxTemp());
        String pressure = weatherViewModel.getPressure() + " " + getString(R.string.pressure_suffix);
        String humidity = (int)weatherViewModel.getHumidity() + getString(R.string.percent_sign);
        String windSpeed = weatherViewModel.getWindSpeed() + " " + getString(R.string.wind_speed_suffix);
        String cloudPercentage = (int)weatherViewModel.getCloudPct() + getString(R.string.percent_sign);

        mTitleTV.setText(cityAndCountry);
        mStatusTV.setText(Utils.capitalizeString(weatherViewModel.getMainDescription()));
        mTempTV.setText(tempurature);
        mTempRangeView.setLabelAndData(getString(R.string.temp_range_label), temperatureRange);
        mPressureView.setLabelAndData(getString(R.string.pressure_label), pressure);
        mHumidityView.setLabelAndData(getString(R.string.humidity_label), humidity);
        mWindSpeedView.setLabelAndData(getString(R.string.wind_speed_label), windSpeed);
        mCloudPctView.setLabelAndData(getString(R.string.cloud_percentage_label), cloudPercentage);
        mSunriseView.setLabelAndData(getString(R.string.sunrise_label), weatherViewModel.getSunriseTime());
        mSunsetView.setLabelAndData(getString(R.string.sunset_label), weatherViewModel.getSunsetTime());
        mDataTimeView.setLabelAndData(getString(R.string.data_recorded_label), weatherViewModel.getLastUpdated());
    }

    private String getTemperatureString(double temp) {
        return Utils.kelvinToCelsius(temp) + getString(R.string.temperature_celsius_suffix);
    }

    private void updateBackgroundAndIcon(String icon) {
        if (icon == null || icon.length() != 3){
            return;
        }

        Picasso.with(this).load(getString(R.string.openweather_icon_engpoint) + "/" + icon + ".png").into(mIconIV); // Image fetch Icon

        IconType iconType = IconType.getTypeFromValue(icon.substring(0, 2));

        if (iconType != null) {
            switch (iconType) {
                case CLEAR_SKY:
                case FEW_CLOUDS:
                case SCATTERED_CLOUDS:
                case BROKEN_CLOUDS:
                    mBackgroundIV.setImageResource(R.drawable.clearsky);
                    break;
                case SHOWER_RAIN:
                case RAIN:
                case THUNDERSTORM:
                case SNOW:
                case MIST:
                    mBackgroundIV.setImageResource(R.drawable.stormysky);
                    break;
                default:
                    break;
            }
            mBackgroundIV.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
    }

    @Override
    protected Location getCurrentLocation() {
        return mCurrentLocation;
    }

    private boolean isLocationPermissionGranted() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkLocationPermission() {
        if (!isLocationPermissionGranted()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_CAMERA);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }
}
