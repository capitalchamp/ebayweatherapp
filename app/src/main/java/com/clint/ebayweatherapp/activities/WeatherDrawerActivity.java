package com.clint.ebayweatherapp.activities;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.clint.ebayweatherapp.R;
import com.clint.ebayweatherapp.servicesapi.RestManager;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.RecentSearch;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.RecentSearches;
import com.clint.ebayweatherapp.utils.Utils;

/**
 * Activity that contains the navigational drawer
 * Created by Clint on 2016-05-14.
 */
public abstract class WeatherDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EDIT_MODE_KEY = "EDIT_MODE_KEY";

    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;

    private EditText searchET;
    private ImageButton submitButton;

    private boolean editMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (mDrawer != null) {
            mDrawer.setDrawerListener(toggle);
            toggle.syncState();
        }

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }

        setUpSearchUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EDIT_MODE_KEY, editMode);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        editMode = savedInstanceState.getBoolean(EDIT_MODE_KEY, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getEditMenuItem().setChecked(editMode);
        handleEditMode();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_gps:
                if (editMode) {
                    editMode = false;
                }
                getEditMenuItem().setChecked(false);

                Location location = getCurrentLocation();
                if (location != null) {
                    RestManager.getInstance().getWeatherByLatLng(location.getLatitude(), location.getLongitude(), (WeatherActivity) this);
                }
                break;
            case R.id.nav_recent1:
            case R.id.nav_recent2:
            case R.id.nav_recent3:
            case R.id.nav_recent4:
            case R.id.nav_recent5:
            case R.id.nav_recent6:
            case R.id.nav_recent7:
            case R.id.nav_recent8:
            case R.id.nav_recent9:
            case R.id.nav_recent10:
                if (editMode) {
                    RecentSearches recentSearches = getRecentSearchesFromPreferences();
                    String title = item.getTitle().toString();
                    String city = title.substring(0, title.indexOf(",")).replace(",", "");
                    recentSearches.removeRecentSearchByCity(city);
                    setRecentSearchesToPreferences(recentSearches);
                } else {
                    getGpsMenuItem().setChecked(false);
                    String cityAndCountry = item.getTitle().toString().trim();
                    cityAndCountry = cityAndCountry.replace(", ", ",");
                    RestManager.getInstance().getWeatherByCityName(cityAndCountry, (WeatherActivity) this);
                }
                break;
            case R.id.nav_edit_recents:
                editMode = !item.isChecked();
                item.setChecked(!item.isChecked());
                break;
        }

        handleEditMode();

        if (mDrawer != null && id != R.id.nav_edit_recents && !editMode) {
            closeDrawer();
        }
        return true;
    }

    private void closeDrawer() {
        hideSoftKeyboard();
        mDrawer.closeDrawer(GravityCompat.START);
    }

    protected Menu getMenu() {
        return mNavigationView.getMenu();
    }

    private MenuItem getGpsMenuItem() {
        return getMenu().findItem(R.id.nav_gps);
    }

    private MenuItem getEditMenuItem() {
        return getMenu().findItem(R.id.nav_edit_recents);
    }

    protected void initRecentSearchesDrawerList(RecentSearches recentSearches) {
        Menu menu = getMenu();
        int count = recentSearches.getCount();

        if (count > 0) {
            activateMenuItem(recentSearches, count - 1, menu.findItem(R.id.nav_recent1));
            activateMenuItem(recentSearches, count - 2, menu.findItem(R.id.nav_recent2));
            activateMenuItem(recentSearches, count - 3, menu.findItem(R.id.nav_recent3));
            activateMenuItem(recentSearches, count - 4, menu.findItem(R.id.nav_recent4));
            activateMenuItem(recentSearches, count - 5, menu.findItem(R.id.nav_recent5));
            activateMenuItem(recentSearches, count - 6, menu.findItem(R.id.nav_recent6));
            activateMenuItem(recentSearches, count - 7, menu.findItem(R.id.nav_recent7));
            activateMenuItem(recentSearches, count - 8, menu.findItem(R.id.nav_recent8));
            activateMenuItem(recentSearches, count - 9, menu.findItem(R.id.nav_recent9));
            activateMenuItem(recentSearches, count - 10, menu.findItem(R.id.nav_recent10));
        } else {
            menu.findItem(R.id.nav_recent1).setVisible(false);
        }
    }

    private void activateMenuItem(RecentSearches recentSearches, int recentSearchIndex, MenuItem item) {
        RecentSearch recentSearch = recentSearches.getRecentSearchAt(recentSearchIndex);
        if (recentSearch != null) {
            String cityAndCountry = recentSearch.getCity() + ", " + recentSearch.getCountry();
            item.setTitle(cityAndCountry);
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
    }

    private void setUpSearchUI() {
        searchET = (EditText) mNavigationView.getHeaderView(0).findViewById(R.id.search_edit_text);
        submitButton = (ImageButton) mNavigationView.getHeaderView(0).findViewById(R.id.submit_button);

        if (submitButton != null) {
            submitButton.setEnabled(false);
        }

        Utils.addMaliciousCharsFilterToEditText(searchET);
        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    submitButton.performClick();
                    return true;
                }
                return false;
            }
        });
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().isEmpty()) {
                    submitButton.setAlpha((float)0.5);
                    submitButton.setEnabled(false);
                } else {
                    submitButton.setAlpha((float)0.8);
                    submitButton.setEnabled(true);
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchQuery = searchET.getText().toString().trim();
                searchET.setText("");
                searchET.clearFocus();
                getGpsMenuItem().setChecked(false);
                if (mDrawer != null) {
                    closeDrawer();
                }
                try {
                    int zipCode = Integer.parseInt(searchQuery);
                    RestManager.getInstance().getWeatherByZipCode(searchQuery, (WeatherActivity) (WeatherDrawerActivity.this));
                } catch (NumberFormatException e) {
                    RestManager.getInstance().getWeatherByCityName(searchQuery, (WeatherActivity) (WeatherDrawerActivity.this));
                }
            }
        });
    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void handleEditMode() {
        if (editMode) {
            getMenu().findItem(R.id.nav_recent1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent3).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent4).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent5).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent6).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent7).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent8).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent9).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
            getMenu().findItem(R.id.nav_recent10).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_clear_black_48dp));
        } else {
            getMenu().findItem(R.id.nav_recent1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent3).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent4).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent5).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent6).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent7).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent8).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent9).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
            getMenu().findItem(R.id.nav_recent10).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_update_black_48dp));
        }
    }

    protected abstract @Nullable Location getCurrentLocation();

    protected abstract RecentSearches getRecentSearchesFromPreferences();
    protected abstract void setRecentSearchesToPreferences(RecentSearches recentSearches);
}
