package com.clint.ebayweatherapp.servicesapi;

import com.clint.ebayweatherapp.servicesapi.models.dtos.DtoWeatherData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Clint on 2016-05-14.
 */
public class RestManager {

    private static RestManager restManager;
    private static RestApi service;

    public static RestManager getInstance() {
        if (restManager == null) {
            restManager = new RestManager();
        }
        return restManager;
    }

    private RestManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestConfig.OPENWEATHER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(RestApi.class);
    }

    public void getWeatherByCityName(String cityName, Callback<DtoWeatherData> callback) {
        Call<DtoWeatherData> call = service.getWeatherByCityName(cityName, RestConfig.OPENWEATHER_API_KEY);
        call.enqueue(callback);
    }

    public void getWeatherByZipCode(String zipCode, Callback<DtoWeatherData> callback) {
        String zipAndCountry = zipCode + ",us"; // Zip codes are only for US

        Call<DtoWeatherData> call = service.getWeatherByZipCode(zipAndCountry, RestConfig.OPENWEATHER_API_KEY);
        call.enqueue(callback);
    }

    public void getWeatherById(int id, Callback<DtoWeatherData> callback) {
        Call<DtoWeatherData> call = service.getWeatherById(id, RestConfig.OPENWEATHER_API_KEY);
        call.enqueue(callback);
    }

    public void getWeatherByLatLng(double lat, double lng, Callback<DtoWeatherData> callback) {
        String latitude, longitude;
        try {
            latitude = Double.toString(lat);
            longitude = Double.toString(lng);
        } catch (Exception e) {
            return;
        }
        if (latitude == null || longitude == null) {
            return;
        }
        Call<DtoWeatherData> call = service.getWeatherByLatLng(latitude, longitude, RestConfig.OPENWEATHER_API_KEY);
        call.enqueue(callback);
    }
}
