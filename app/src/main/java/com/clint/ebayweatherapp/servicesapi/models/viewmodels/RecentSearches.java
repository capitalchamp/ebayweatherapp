package com.clint.ebayweatherapp.servicesapi.models.viewmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clint on 2016-05-14.
 */
public class RecentSearches implements Serializable {

    @SerializedName("recentSearches")
    private List<RecentSearch> recentSearches;

    public RecentSearches() {
        this.recentSearches = new ArrayList<>();
    }

    public void addRecentSearch(RecentSearch recentSearch) {
        if (recentSearches == null) {
            recentSearches = new ArrayList<>();
        }

        // Will max it out at 10 recent searches
        if (recentSearches.size() >= 10) {
            removeRecentSearch(recentSearches.get(0));
        }

        if (containsRecentSearch(recentSearch)) {
            removeRecentSearch(recentSearch);
        }

        recentSearches.add(recentSearch);
    }

    private boolean containsRecentSearch(RecentSearch recentSearch) {
        for (RecentSearch rs : recentSearches) {
            if (rs.getId() == recentSearch.getId()) {
                return true;
            }
        }
        return false;
    }

    public void removeRecentSearch(RecentSearch recentSearch) {
        if (recentSearches != null && containsRecentSearch(recentSearch)) {
            for (RecentSearch rs : recentSearches) {
                if (rs.getId() == recentSearch.getId()) {
                    recentSearch = rs;
                    break;
                }
            }
            recentSearches.remove(recentSearch);
        }
    }

    public void removeRecentSearchByCity(String city) {
        if (recentSearches != null) {
            RecentSearch recentSearch = null;
            for (RecentSearch rs : recentSearches) {
                if (rs.getCity().equals(city)) {
                    recentSearch = rs;
                    break;
                }
            }
            if (recentSearch != null) {
                recentSearches.remove(recentSearch);
            }
        }
    }

    public RecentSearch getMostRecentSearch() {
        if (recentSearches.size() <= 0) {
            return null;
        }
        return recentSearches.get(recentSearches.size() - 1);
    }

    public RecentSearch getRecentSearchAt(int i) {
        if (i < 0 || i >= recentSearches.size()) {
            return null;
        }
        return recentSearches.get(i);
    }

    public int getCount() {
        return recentSearches.size();
    }
}
