package com.clint.ebayweatherapp.servicesapi.models.viewmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Clint on 2016-05-14.
 */
public class RecentSearch {
    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("id")
    private int id;

    public RecentSearch(String city, String country, int id) {
        this.city = city;
        this.country = country;
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getId() {
        return id;
    }
}
