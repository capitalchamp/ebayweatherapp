package com.clint.ebayweatherapp.servicesapi.models.dtos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Weather object returned from API
 *
 * Created by Clint on 2016-05-14.
 */
public class DtoWeatherData {

    @SerializedName("coord")
    private LatLng coord;

    @SerializedName("weather")
    private List<Weather> weather;

    @SerializedName("base")
    private String base;

    @SerializedName("main")
    private MainData main;

    @SerializedName("wind")
    private WindData wind;

    @SerializedName("rain")
    private RainData rain;

    @SerializedName("clouds")
    private CloudData clouds;

    @SerializedName("dt")
    private long dt;

    @SerializedName("sys")
    private SysData sys;

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("cod")
    private int cod;

    public LatLng getCoord() {
        return coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public String getBase() {
        return base;
    }

    public MainData getMain() {
        return main;
    }

    public WindData getWind() {
        return wind;
    }

    public RainData getRain() {
        return rain;
    }

    public CloudData getClouds() {
        return clouds;
    }

    public long getDt() {
        return dt;
    }

    public SysData getSys() {
        return sys;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCod() {
        return cod;
    }

    public class Weather {
        @SerializedName("id")
        private int id;

        @SerializedName("main")
        private String main;

        @SerializedName("description")
        private String description;

        @SerializedName("icon")
        private String icon;

        public int getId() {
            return id;
        }

        public String getMain() {
            return main;
        }

        public String getDescription() {
            return description;
        }

        public String getIcon() {
            return icon;
        }
    }

    public class LatLng {
        @SerializedName("lon")
        private double lng;

        @SerializedName("lat")
        private double lat;

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }
    }

    public class MainData {
        @SerializedName("temp")
        private double temp;

        @SerializedName("pressure")
        private double pressure;

        @SerializedName("humidity")
        private double humidity;

        @SerializedName("temp_min")
        private double temp_min;

        @SerializedName("temp_max")
        private double temp_max;

        @SerializedName("sea_level")
        private double sea_level;

        @SerializedName("grnd_level")
        private double grnd_level;

        public double getTemp() {
            return temp;
        }

        public double getPressure() {
            return pressure;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getTemp_min() {
            return temp_min;
        }

        public double getTemp_max() {
            return temp_max;
        }

        public double getSea_level() {
            return sea_level;
        }

        public double getGrnd_level() {
            return grnd_level;
        }
    }

    public class WindData {
        @SerializedName("speed")
        private double speed;

        @SerializedName("deg")
        private double deg;

        public double getSpeed() {
            return speed;
        }

        public double getDeg() {
            return deg;
        }
    }

    public class RainData {
        @SerializedName("3h")
        private double threeHour;

        public double getThreeHour() {
            return threeHour;
        }
    }

    public class CloudData {
        @SerializedName("all")
        private double all;

        public double getAll() {
            return all;
        }
    }

    public class SysData {
        @SerializedName("message")
        private double message;

        @SerializedName("country")
        private String country;

        @SerializedName("sunrise")
        private long sunrise;

        @SerializedName("sunset")
        private long sunset;

        public double getMessage() {
            return message;
        }

        public String getCountry() {
            return country;
        }

        public long getSunrise() {
            return sunrise;
        }

        public long getSunset() {
            return sunset;
        }
    }
}
