package com.clint.ebayweatherapp.servicesapi;

import com.clint.ebayweatherapp.servicesapi.models.dtos.DtoWeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Clint on 2016-05-14.
 */
public interface RestApi {
    @GET("weather")
    Call<DtoWeatherData> getWeatherByCityName(@Query("q") String cityName, @Query("appid") String apiKey);

    @GET("weather")
    Call<DtoWeatherData> getWeatherByZipCode(@Query("zip") String zipAndCountryCode, @Query("appid") String apiKey);

    @GET("weather")
    Call<DtoWeatherData> getWeatherById(@Query("id") int id, @Query("appid") String apiKey);

    @GET("weather")
    Call<DtoWeatherData> getWeatherByLatLng(@Query("lat") String lat, @Query("lon") String lng, @Query("appid") String apiKey);
}
