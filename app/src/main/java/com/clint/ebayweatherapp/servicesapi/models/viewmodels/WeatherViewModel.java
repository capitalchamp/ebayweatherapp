package com.clint.ebayweatherapp.servicesapi.models.viewmodels;

import java.io.Serializable;

/**
 *
 * Created by Clint on 2016-05-14.
 */
public class WeatherViewModel implements Serializable {

    private String city;
    private String country;

    private String mainSummary; //Rain
    private String mainDescription; //Light Rain
    private String icon;

    private double temp = Double.NaN;
    private double pressure = Double.NaN;
    private double humidity = Double.NaN;
    private double minTemp = Double.NaN;
    private double maxTemp = Double.NaN;

    private double windSpeed = Double.NaN;
    private double cloudPct = Double.NaN;

    private String lastUpdated;
    private String sunriseTime;
    private String sunsetTime;

    private WeatherViewModel(WeatherViewModelBuilder builder) {
        this.city = builder.city;
        this.country = builder.country;
        this.mainSummary = builder.mainSummary;
        this.mainDescription = builder.mainDescription;
        this.temp = builder.temp;
        this.pressure = builder.pressure;
        this.humidity = builder.humidity;
        this.minTemp = builder.minTemp;
        this.maxTemp = builder.maxTemp;
        this.windSpeed = builder.windSpeed;
        this.lastUpdated = builder.lastUpdated;
        this.sunriseTime = builder.sunriseTime;
        this.sunsetTime = builder.sunsetTime;
        this.icon = builder.icon;
        this.cloudPct = builder.cloudPct;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getMainSummary() {
        return mainSummary;
    }

    public String getMainDescription() {
        return mainDescription;
    }

    public double getTemp() {
        return temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getSunriseTime() {
        return sunriseTime;
    }

    public String getSunsetTime() {
        return sunsetTime;
    }

    public String getIcon() {
        return icon;
    }

    public double getPressure() {
        return pressure;
    }

    public double getCloudPct() {
        return cloudPct;
    }

    public static class WeatherViewModelBuilder {
        String city;
        String country;
        String mainSummary; //Rain
        String mainDescription; //Light Rain
        String icon;
        double temp;
        double pressure;
        double humidity;
        double minTemp;
        double maxTemp;
        double windSpeed;
        double cloudPct;
        String lastUpdated;
        String sunriseTime;
        String sunsetTime;

        public WeatherViewModelBuilder setCity(String city) {
            this.city = city;
            return this;
        }

        public WeatherViewModelBuilder setCountry(String country) {
            this.country = country;
            return this;
        }

        public WeatherViewModelBuilder setMainSummary(String mainSummary) {
            this.mainSummary = mainSummary;
            return this;
        }

        public WeatherViewModelBuilder setMainDescription(String mainDescription) {
            this.mainDescription = mainDescription;
            return this;
        }

        public WeatherViewModelBuilder setTemp(double temp) {
            this.temp = temp;
            return this;
        }

        public WeatherViewModelBuilder setHumidity(double humidity) {
            this.humidity = humidity;
            return this;
        }

        public WeatherViewModelBuilder setMinTemp(double minTemp) {
            this.minTemp = minTemp;
            return this;
        }

        public WeatherViewModelBuilder setMaxTemp(double maxTemp) {
            this.maxTemp = maxTemp;
            return this;
        }

        public WeatherViewModelBuilder setWindSpeed(double windSpeed) {
            this.windSpeed = windSpeed;
            return this;
        }

        public WeatherViewModelBuilder setLastUpdated(String lastUpdated) {
            this.lastUpdated = lastUpdated;
            return this;
        }

        public WeatherViewModelBuilder setSunriseTime(String sunriseTime) {
            this.sunriseTime = sunriseTime;
            return this;
        }

        public WeatherViewModelBuilder setSunsetTime(String sunsetTime) {
            this.sunsetTime = sunsetTime;
            return this;
        }

        public WeatherViewModelBuilder setIcon(String icon) {
            this.icon = icon;
            return this;
        }

        public WeatherViewModelBuilder setPressure(double pressure) {
            this.pressure = pressure;
            return this;
        }

        public WeatherViewModelBuilder setCloudPct(double cloudPct) {
            this.cloudPct = cloudPct;
            return this;
        }

        public WeatherViewModel build() {
            return new WeatherViewModel(this);
        }
    }
}
