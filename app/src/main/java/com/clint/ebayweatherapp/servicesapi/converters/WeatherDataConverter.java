package com.clint.ebayweatherapp.servicesapi.converters;

import com.clint.ebayweatherapp.servicesapi.models.dtos.DtoWeatherData;
import com.clint.ebayweatherapp.servicesapi.models.viewmodels.WeatherViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Clint on 2016-05-14.
 */
public class WeatherDataConverter {

    public static WeatherViewModel createWeatherViewModel(DtoWeatherData weatherData) {
        String mainSummary = null, mainDescription = null, icon = null;
        if (weatherData.getWeather().get(0) != null) {
            mainSummary = weatherData.getWeather().get(0).getMain();
            mainDescription = weatherData.getWeather().get(0).getDescription();
            icon = weatherData.getWeather().get(0).getIcon();
        }

        return new WeatherViewModel.WeatherViewModelBuilder()
                .setCity(weatherData.getName())
                .setCountry(weatherData.getSys().getCountry())
                .setPressure(weatherData.getMain().getPressure())
                .setHumidity(weatherData.getMain().getHumidity())
                .setCloudPct(weatherData.getClouds().getAll())
                .setMainSummary(mainSummary)
                .setMainDescription(mainDescription)
                .setIcon(icon)
                .setLastUpdated(getLastUpdatedFriendlyString(weatherData.getDt()))
                .setTemp(weatherData.getMain().getTemp())
                .setMinTemp(weatherData.getMain().getTemp_min())
                .setMaxTemp(weatherData.getMain().getTemp_max())
                .setSunriseTime(getTimeOfDayFriendlyString(weatherData.getSys().getSunrise()))
                .setSunsetTime(getTimeOfDayFriendlyString(weatherData.getSys().getSunset()))
                .setWindSpeed(weatherData.getWind().getSpeed())
                .build();
    }

    private static String getTimeOfDayFriendlyString(long time) {
        //comes as seconds since 1970
        Date date = new Date(time*1000);
        SimpleDateFormat sdf = (SimpleDateFormat)SimpleDateFormat.getTimeInstance();
        sdf.applyPattern("h:mm a");
        return sdf.format(date);
    }

    private static String getLastUpdatedFriendlyString(long lastUpdated) {
        //comes as seconds since 1970
        Date date = new Date(lastUpdated*1000);
        SimpleDateFormat sdf = (SimpleDateFormat)SimpleDateFormat.getDateTimeInstance();
        sdf.applyPattern("yyyy-MM-dd h:mm a");
        return sdf.format(date);
    }
}
