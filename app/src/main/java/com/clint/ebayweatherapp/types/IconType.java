package com.clint.ebayweatherapp.types;

/**
 * Created by Clint on 2016-05-15.
 */
public enum IconType {
    CLEAR_SKY("01"),
    FEW_CLOUDS("02"),
    SCATTERED_CLOUDS("03"),
    BROKEN_CLOUDS("04"),
    SHOWER_RAIN("09"),
    RAIN("10"),
    THUNDERSTORM("11"),
    SNOW("13"),
    MIST("50");

    private String value;

    IconType(String value) {
        this.value = value;
    }

    public static IconType getTypeFromValue(String value) {
        for (IconType iconType : values()) {
            if (iconType.value.equals(value)) {
                return iconType;
            }
        }
        return null;
    }
}
