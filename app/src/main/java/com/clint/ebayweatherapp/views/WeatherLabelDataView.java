package com.clint.ebayweatherapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clint.ebayweatherapp.R;

/**
 * Created by Clint on 2016-05-15.
 */
public class WeatherLabelDataView extends LinearLayout {

    private TextView mLabelTV;
    private TextView mDataTV;

    public WeatherLabelDataView(Context context) {
        super(context);
        inflate(getContext(), R.layout.view_weather_label_data, this);
    }

    public WeatherLabelDataView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.view_weather_label_data, this);
    }

    public WeatherLabelDataView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_weather_label_data, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLabelTV = (TextView) findViewById(R.id.label);
        mDataTV = (TextView) findViewById(R.id.data);
    }

    public void setLabelAndData(String label, String data) {
        if (label != null) {
            mLabelTV.setText(label);
        }

        if (data != null) {
            mDataTV.setText(data);
        }
    }
}
