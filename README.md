# README #

This is a Weather App made by Clint Deygoo for the eBay Interview Process.

Features:

* displaying key weather data points for numerous cities around the globe

* search allows for cities and zip codes

* user can fetch weather data for their current location

* saves recent search history, which can be quickly recovered

* localized app name to Canada and Australia

* incorporates Marshmallow's new permission model